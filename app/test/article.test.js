process.env.NODE_ENV = 'test';

const chai = require('chai');
const request = require('supertest');
const { server } = require('../../server.js');
const { updateData } = require('../controllers/article.controller.js');

const { expect } = chai;

const true_article = {
    id:2,
    title: "Node.js – v18.2.0",
    url: "https://nodejs.org/en/blog/release/v18.2.0/",
    author: "bricss",
    created_at: "2022-05-17T16:01:17.000Z",
    points: 3,
    story_text: null,
    comment_text: null,
    num_comments: 0,
    story_id: null,
    story_title: null,
    story_url: null,
    parent_id: null,
    created_at_i:1652803277,
    objectID: 31412616,
    createdAt: "2022-05-21T18:18:31.000Z",
    updatedAt: "2022-05-21T18:18:31.000Z",
    tags: [ 
        {
            "name":"story"
        }, 
        {
            "name":"author_bricss"
        }, 
        {
            "name":"story_31412616"
        }, ]
   }

describe('Test: Get all articles', async () => {
    it('Shows all articles', async () => {
        const { body, status } = await request(server).get('/api/articles');
        console.log(body);
        expect(status).to.equal(200);

    });
});

describe('Test: Get article with author', async () => {
    it('Gets the article with the author bricss', async () => {
        const { body, status } = await request(server).get('/api/articles?author=bricss');
        console.log(body);
        expect(status).to.equal(200);

    });
});

describe('Test: Get article with title', async () => {
    it('Gets the article with specific title', async () => {
        const title = encodeURI("Node.js – v18.2.0");
        const { body, status } = await request(server).get('/api/articles?title='+title);
        console.log(body);
        expect(status).to.equal(200);

    });
});

describe('Test: Get article with tag', async () => {
    it('Gets the article with the title Node.js – v18.2.0', async () => {
        const { body, status } = await request(server).get('/api/articlesByTag?tag=comment');
        console.log(body);
        expect(status).to.equal(200);

    });
});

describe('Test: Get tags', async () => {
    it('Gets only the first 5 tags', async () => {
        const { body, status } = await request(server).get('/api/tags');
        console.log(body);
        expect(status).to.equal(200);

    });
});

describe('Test: Get tags 2', async () => {
    it('Gets the second page of tags (6-10)', async () => {
        const { body, status } = await request(server).get('/api/tags?page=1');
        console.log(body);
        expect(status).to.equal(200);

    });
});

describe('Test: request HN API', async () => {
    it('requests the last 20 articles about node from the HN API and update the database with them', async () => {
        await updateData();
        const { body, status } = await request(server).get('/api/articles');
        console.log(body);
        expect(status).to.equal(200);

    });
});

describe('Delete article test', async () => {
    it('Deletes article with id 1', async () => {
        const { body, status } = await request(server).delete('/api/articles/1');
        console.log(body);
        expect(status).to.equal(200);

    });
});