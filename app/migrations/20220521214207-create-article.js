'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Articles', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      author: {
        type: Sequelize.STRING
      },
      title: Sequelize.STRING,
      url: Sequelize.STRING,
      author: Sequelize.STRING,
      created_at: Sequelize.DATE,
      points: Sequelize.STRING,
      story_text: Sequelize.TEXT,
      comment_text: Sequelize.TEXT,
      num_comments: Sequelize.INTEGER,
      story_id: Sequelize.INTEGER,
      story_title: Sequelize.STRING,
      story_url: Sequelize.STRING,
      parent_id: Sequelize.INTEGER,
      created_at_i: Sequelize.INTEGER,
      objectID: {
        type: Sequelize.INTEGER,
        unique: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Articles');
  }
};