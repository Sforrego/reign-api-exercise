const { article_tags } = require("../models");
const db = require("../models");
const Article = db.article;
const Tag = db.tag;

exports.create = (tag) => {
    return Tag.create({
      name: tag.name,
    })
      .then((tag) => {
        console.log(">> Created Tag: " + JSON.stringify(tag, null, 2));
        return tag;
      })
      .catch((err) => {
        console.log(">> Error while creating Tag: ", err);
      });
  };

exports.findAll = (req, res) => {  
  const page = req.query.page;
    return Tag.findAll({
      include: [
        {
          model: Article,
          as: "articles",
          attributes: ["id", "title", "author"],
          through: {
            attributes: [],
          }
        },
      ],
      limit: 5,
      offset: page ? 5*page : 0
    })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send(">> Error while retrieving Tags: ", err);
      });
  };
  
exports.findById = (req, res) => {
    const id = req.params.id;
    return Tag.findByPk(id, {
      include: [
        {
          model: Article,
          as: "articles",
          attributes: ["id", "title", "author"],
          through: {
            attributes: [],
          }
        },
      ],
    })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({message:">> Error while finding Tag: ", err});
      });
  };

exports.findByName = (req, res) => {
    const name = req.params.name;
    return Tag.findOne({
      where: {name: name},
      include: [
        {
          model: Article,
          as: "articles",
          attributes: ["id", "title", "author"],
          through: {
            attributes: [],
          }
        },
      ],
    })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({message:">> Error while finding Tag: ", err});
      });
  };

exports.addArticle = (tagId, articleId, tagName) => {
  console.log(`TAGNAME: ${tagName}`);
    return Tag.findByPk(tagId)
      .then((tag) => {
        if (!tag) {
          console.log("Tag not found!");
          return null;
        }
        return Article.findByPk(articleId).then((article) => {
          if (!article) {
            console.log("Article not found!");
            return null;
          }
          article_tags.create({article_id:articleId,tag_id:tagId,name:tagName});
          // tag.addArticle(article, {through: {name:tagName}});
          console.log(`>> added Article id=${article.id} to Tag id=${tag.id}`);
          return tag;
        });
      })
      .catch((err) => {
        console.log(">> Error while adding Article to Tag: ", err);
      });
  };