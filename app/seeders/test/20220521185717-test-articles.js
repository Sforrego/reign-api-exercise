'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {

     await queryInterface.bulkInsert('Articles', 
     [
       {
      id:1,
      title: null,
      url: null,
      author: "Blahah",
      created_at: "2022-05-21T18:18:31.000Z",
      points: null,
      story_text: null,
      comment_text: "It&#x27;s not insider information because the information is accessible and discoverable. Anyone who wants to can follow the development plans, run their own nodes, pull and build the new branches, etc.<p>There&#x27;s certainly a lot of insider trading (adjacent) bad behaviour in crypto generally, but the example you&#x27;ve given is the opposite.",
      num_comments: null,
      story_id: 31458021,
      story_title:"Crypto Might Have an Insider Trading Problem",
      story_url: "https://www.wsj.com/articles/crypto-might-have-an-insider-trading-problem-11653084398",
      parent_id: 31459654,
      created_at_i:1653157111,
      objectID: 31460393,
      createdAt: "2022-05-21T18:18:31.000Z",
      updatedAt: "2022-05-21T18:18:31.000Z"
     },
       {
      id:2,
      title: "Node.js – v18.2.0",
      url: "https://nodejs.org/en/blog/release/v18.2.0/",
      author: "bricss",
      created_at: "2022-05-17T16:01:17.000Z",
      points: 3,
      story_text: null,
      comment_text: null,
      num_comments: 0,
      story_id: null,
      story_title: null,
      story_url: null,
      parent_id: null,
      created_at_i:1652803277,
      objectID: 31412616,
      createdAt: "2022-05-21T18:18:31.000Z",
      updatedAt: "2022-05-21T18:18:31.000Z"
     },

    ],
     {}
     );
  
     
    await queryInterface.bulkInsert('Tags', 
    [

      {
       id: 1,
       name: "comment",
       createdAt: "2022-05-21T18:18:31.000Z",
       updatedAt: "2022-05-21T18:18:31.000Z"
      },
      {
       id: 2,
       name: "author_Blahah",
       createdAt: "2022-05-21T18:18:31.000Z",
       updatedAt: "2022-05-21T18:18:31.000Z"
      },
      {
       id: 3,
       name: "story_31458021",
       createdAt: "2022-05-21T18:18:31.000Z",
       updatedAt: "2022-05-21T18:18:31.000Z"
      },
      {
       id: 4,
       name: "story",
       createdAt: "2022-05-21T18:18:31.000Z",
       updatedAt: "2022-05-21T18:18:31.000Z"
      },
      {
       id: 5,
       name: "author_bricss",
       createdAt: "2022-05-21T18:18:31.000Z",
       updatedAt: "2022-05-21T18:18:31.000Z"
      },
      {
       id: 6,
       name: "story_31412616",
       createdAt: "2022-05-21T18:18:31.000Z",
       updatedAt: "2022-05-21T18:18:31.000Z"
      }
    ],
    {}
    );

     await queryInterface.bulkInsert('ArticleTags', 
     [
      {
        article_id:1,
        tag_id:1,
        name:"comment",
        createdAt: "2022-05-21T18:18:31.000Z",
        updatedAt: "2022-05-21T18:18:31.000Z"
      },
      {
        article_id:1,
        tag_id:2,
        name:"author_Blahah",
        createdAt: "2022-05-21T18:18:31.000Z",
        updatedAt: "2022-05-21T18:18:31.000Z"
      },
      {
        article_id:1,
        tag_id:3,
        name:"story_31458021",
        createdAt: "2022-05-21T18:18:31.000Z",
        updatedAt: "2022-05-21T18:18:31.000Z"
      },
      {
        article_id:2,
        tag_id:4,
        name:"story",
        createdAt: "2022-05-21T18:18:31.000Z",
        updatedAt: "2022-05-21T18:18:31.000Z"
      },
      {
        article_id:2,
        tag_id:5,
        name:"authos_bricss",
        createdAt: "2022-05-21T18:18:31.000Z",
        updatedAt: "2022-05-21T18:18:31.000Z"
      },
      {
        article_id:2,
        tag_id:6,
        name:"story_31412616",
        createdAt: "2022-05-21T18:18:31.000Z",
        updatedAt: "2022-05-21T18:18:31.000Z"
      },
     ],
     {}
     );
     
  },

  async down (queryInterface, Sequelize) {
    


     await queryInterface.bulkDelete('Articles', null, {});
     await queryInterface.bulkDelete('Tags', null, {});
     await queryInterface.bulkDelete('ArticleTags', null, {});

  }
};
