
const environment = process.env.NODE_ENV ? "test" : "development";
const dbConfig = require("../config/config.js")[environment];
const Sequelize = require("sequelize");

const sequelize = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, {
  host: dbConfig.host,
  dialect: dbConfig.dialect,
});
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.article = require("./article.js")(sequelize, Sequelize);
db.tag = require("./tag.js")(sequelize, Sequelize);
db.article_tags = require("./article_tags.js")(sequelize, Sequelize);

db.tag.belongsToMany(db.article, {
  through: "ArticleTags",
  as: "articles",
  foreignKey: "tag_id",
});
db.article.belongsToMany(db.tag, {
  through: "ArticleTags",
  as: "tags",
  foreignKey: "article_id",
});

module.exports = db;