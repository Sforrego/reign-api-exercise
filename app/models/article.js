'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Article extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Article.init({
    author: DataTypes.STRING,
    title: DataTypes.STRING,
    url: DataTypes.STRING,
    author: DataTypes.STRING,
    created_at: DataTypes.DATE,
    points: DataTypes.STRING,
    story_text: DataTypes.TEXT,
    comment_text: DataTypes.TEXT,
    num_comments: DataTypes.INTEGER,
    story_id: DataTypes.INTEGER,
    story_title: DataTypes.STRING,
    story_url: DataTypes.STRING,
    parent_id: DataTypes.INTEGER,
    created_at_i: DataTypes.INTEGER,
    objectID: {
      type: DataTypes.INTEGER,
      unique: true}
  }, {
    sequelize,
    modelName: 'Article',
  });
  return Article;
};