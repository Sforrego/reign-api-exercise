module.exports = app => {
    const articles = require("../controllers/article.controller.js");
    const tags = require("../controllers/tag.controller.js");
    var router = require("express").Router();
    // not sure if keeping these two
    // Create a new Article
    router.post("/articles/", articles.create);
    // Update a Article with id
    router.put("/articles/:id", articles.update); 


    // Retrieve all Articles
    router.get("/articles/", articles.findAll);
    // Retrieve all Articles
    router.get("/articlesBytag", articles.findAllbyTag);
    // // Retrieve all Articles, could be use to differentiate articles and comments
    // router.get("/published", articles.findAllPublished);
    // Retrieve a single Article with id
    router.get("/articles/:id", articles.findOne);
    // Delete a Article with id
    router.delete("/articles/:id", articles.delete);
    // Create a new Article
    router.delete("/articles/", articles.deleteAll);

    // Retrieve all tags
    router.get("/tags/", tags.findAll);
    router.get("/tags/:name", tags.findByName);


    app.use('/api', router);
  };