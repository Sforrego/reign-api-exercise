module.exports = {
    development: {
      host: "localhost",
      username: "me",
      password: "999999999",
      database: "reign_api",
      dialect: "postgres",
    },
    test: {
      host: "localhost",
      username: "me",
      password: "999999999",
      database: "reign_api_test",
      dialect: "postgres",
  },
    // no production, because it is not needed for this exercise
  };