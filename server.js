const express = require("express");
const bodyParser = require("body-parser");

const app = express();
const server = require('http').Server(app);
const routes = require('./app/routes/article.routes.js');
const { updateData } = require("./app/controllers/article.controller");

app.use(bodyParser.json());

routes(app);

// update db with latest posts
if (!(process.env.NODE_ENV=="test")){
  updateData(); // will populate the database
  setInterval(updateData,3600000); // 1 hour interval
}

// set port, listen for requests
const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

module.exports = {
  app,
  server,
};