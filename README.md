## REIGN API EXERCISE

## Description

This project consists in a REST API developed with Node.js + Express jS that connects hourly to the Hacker News API with https://hn.algolia.com/api/v1/search_by_date?query=nodejs to get the recently posted articles about Nodejs, it inserts the data into a PostreSQL database using Sequelize as ORM.  

## How to run

Run postgresql and create user with `CREATE USER me WITH PASSWORD '999999999';` and give him permissions with `ALTER USER me WITH CREATEDB;`, then create the database `CREATE DATABASE reign_api` matching the information in app/config/config.js, you can also create the database running `npx sequelize-cli db:create` in the root folder terminal.

After creating the database, run `npm install`, then migrate and seed with 
`npx sequelize-cli db:migrate` and  `npx sequelize-cli db:seed:all` and finally start the server with `node server.js` (npm start could be used if more command where needed) from root folder. The server should populate the db with the last 20 articles created in HN about nodejs (in addition to the seeds), and will fetch for more articles every hour.

## How to use

Using a client like Postman you can use the following requests:

GET localhost:3000/api/articles
This will retrieve the last 5 articles added
with the following keys to filter: title, author, page

DELETE localhost:3000/api/articles/:id

GET localhost:3000/api/articlesByTag with key tag to find the articles with the matching tag.

For better understanding there is a postman file with all the examples.

## Modeling

Two relations where created: Article and Tag, these have a many to many relationship between them, this way it does not violate the first normal form by adding multiple values (tags) to a single column (_tags from article, which is not there anymore).

## Testing

Use `npm test` for testing, this will load the testing environment, reset the database and populate it from the seed in app/seeders/test.

To see the coverage `npx nyc npm test` does the job.

## Future Work

I would have liked to separate articles into two models, Articles and Comments. 

More and better tests, I was quite vague with the tests that were made, checking mainly for status when there should be more specific values that can be tested, but because of time were not. 

Usage of typescript, I do enjoy good typed programming but started with regular js and did not have time to migrate.



